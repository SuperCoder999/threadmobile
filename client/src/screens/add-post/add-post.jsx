import * as React from 'react';
import {
  HomeScreenName,
  TextVariant
} from 'common/enums/enums';
import { Text, View, ModifyPostForm } from 'components/components';
import { useDispatch, useNavigation } from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';
import styles from './styles';

const AddPost = () => {
  const dispatch = useDispatch();
  const navigator = useNavigation();

  const handleAddPost = payload => {
    dispatch(threadActionCreator.createPost(payload));
    navigator.navigate(HomeScreenName.THREAD);
  };

  return (
    <View style={styles.screen}>
      <Text variant={TextVariant.HEADLINE}>Add Post</Text>
      <ModifyPostForm onSubmit={handleAddPost} />
    </View>
  );
};

export default AddPost;
