import { HttpCode, ExceptionName } from '../../../common/enums/enums';

const getErrorStatusCode = err => {
  const hasNameProp = Object.prototype.hasOwnProperty.call(err, 'name');
  if (!hasNameProp) {
    return HttpCode.INTERNAL_SERVER_ERROR;
  }

  switch (err.name) {
    case ExceptionName.INVALID_CREDENTIALS:
    case ExceptionName.INVALID_RESET_PASSWORD_TOKEN: {
      return HttpCode.UNAUTHORIZED;
    }
    case ExceptionName.NOT_POST_AUTHOR: {
      return HttpCode.FORBIDDEN;
    }
    case ExceptionName.NOT_FOUND: {
      return HttpCode.NOT_FOUND;
    }
    default: {
      return HttpCode.INTERNAL_SERVER_ERROR;
    }
  }
};

export { getErrorStatusCode };
