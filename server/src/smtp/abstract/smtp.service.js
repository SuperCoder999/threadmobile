import nodemailer from 'nodemailer';
import { formatString } from '../../helpers/helpers';

class SMTP {
  constructor(username, mask, config) {
    this.username = username;
    this.mask = mask;
    this.config = config;

    this._connect();
  }

  _getFromAddress() {
    return this.mask ? `${this.mask}<${this.username}>` : this.username;
  }

  _connect() {
    if (this.connection) {
      return;
    }

    this.connection = nodemailer.createTransport(this.config);
  }

  send(to, subject, text) {
    return this.connection.sendMail({
      to,
      subject,
      text
    });
  }

  sendHtml(to, subject, html) {
    return this.connection.sendMail({
      to,
      subject,
      html
    });
  }

  sendTemplate(to, template, variables) {
    const subject = formatString(template.subject, variables);
    const body = formatString(template.body, variables);

    return this.sendHtml(to, subject, body);
  }
}

export { SMTP };
