class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  update(id, data) {
    return this._commentRepository.updateById(id, data);
  }

  delete(id) {
    return this._commentRepository.deleteById(id);
  }

  async setReaction(userId, { commentId, isLike = true }) {
    const updateOrDelete = react => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({ userId, commentId, isLike });

    const isDeleted = Number.isInteger(result);

    return {
      updated: Boolean(reaction) && reaction.isLike !== isLike,
      reaction: isDeleted
        ? {}
        : await this._commentReactionRepository.getCommentReaction(userId, commentId)
    };
  }
}

export { Comment };
