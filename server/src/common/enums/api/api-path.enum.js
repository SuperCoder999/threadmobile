const ApiPath = {
  AUTH: '/auth',
  PROFILE: '/profile',
  POSTS: '/posts',
  COMMENTS: '/comments',
  IMAGES: '/images',
  DEEP_LINKS: '/deep-links'
};

export { ApiPath };
