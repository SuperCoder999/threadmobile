import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { AuthScreenName, AuthFormType } from 'common/enums/enums';
import RequestResetPassword from 'screens/request-reset-password/request-reset-password';
import ResetPassword from 'screens/reset-password/reset-password';
import LoginRegister from 'screens/auth/auth';

const screenOptions = {
  headerShown: false,
  ...TransitionPresets.ScaleFromCenterAndroid
};

const Stack = createStackNavigator();

const Auth = () => (
  <Stack.Navigator screenOptions={screenOptions}>
    <Stack.Screen
      name={AuthScreenName.LOGIN_REGISTER}
      component={LoginRegister}
      initialParams={{ formType: AuthFormType.LOGIN }}
    />
    <Stack.Screen name={AuthScreenName.REQUEST_RESET} component={RequestResetPassword} />
    <Stack.Screen name={AuthScreenName.RESET} component={ResetPassword} />
  </Stack.Navigator>
);

export default Auth;
