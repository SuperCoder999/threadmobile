const DbTableName = {
  COMMENTS: 'comments',
  IMAGES: 'images',
  POST_REACTIONS: 'postReactions',
  COMMENT_REACTIONS: 'commentReactions',
  POSTS: 'posts',
  USERS: 'users'
};

export { DbTableName };
