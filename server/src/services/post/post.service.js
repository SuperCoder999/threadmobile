import { EmailTemplate } from '../../common/enums/enums';
import { NotPostAuthorError } from '../../exceptions/exceptions';
import { getDeepLink } from '../../helpers/helpers';
import { celery } from '../../celery/client/client';

class Post {
  constructor({ userRepository, postRepository, postReactionRepository }) {
    this._userRepository = userRepository;
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async update(postId, data, currentUserId) {
    const userId = await this._postRepository.getPostUserId(postId);

    if (userId !== currentUserId) {
      throw new NotPostAuthorError();
    }

    return this._postRepository.updateById(postId, data);
  }

  async delete(postId, currentUserId) {
    const userId = await this._postRepository.getPostUserId(postId);

    if (userId !== currentUserId) {
      throw new NotPostAuthorError();
    }

    return this._postRepository.deleteById(postId);
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    const isDeleted = Number.isInteger(result);

    if (isLike && !isDeleted) {
      const post = await this._postRepository.getById(postId);
      const newReaction = reaction ?? result;

      if (newReaction.userId !== post.userId) {
        const postUser = await this._userRepository.getById(post.userId);
        const reactionUser = await this._userRepository.getById(newReaction.userId);
        const link = getDeepLink(`liked-post/${post.id}`);

        celery.runTask('sendEmail', {
          to: postUser.email,
          template: EmailTemplate.LIKE,
          variables: {
            username: postUser.username,
            likerUsername: reactionUser.username,
            link
          }
        });
      }
    }

    return {
      updated: Boolean(reaction) && reaction.isLike !== isLike,
      reaction: isDeleted
        ? {}
        : await this._postReactionRepository.getPostReaction(userId, postId)
    };
  }
}

export { Post };
