import { HttpMethod, ContentType } from 'common/enums/enums';

class Auth {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  login(payload) {
    return this._http.load(`${this._apiPath}/auth/login`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load(`${this._apiPath}/auth/register`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  requestResetPassword(email) {
    return this._http.load(`${this._apiPath}/auth/reset-password/request`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify({ email })
    });
  }

  resetPassword(payload) {
    return this._http.load(`${this._apiPath}/auth/reset-password`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  checkResetPasswordToken(token) {
    return this._http.load(`${this._apiPath}/auth/reset-password/check`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify({ token })
    });
  }

  getCurrentUser() {
    return this._http.load(`${this._apiPath}/auth/user`, {
      method: HttpMethod.GET
    });
  }
}

export { Auth };
