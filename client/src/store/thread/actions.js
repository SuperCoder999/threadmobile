import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.updatePost(post.id, post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { extra: { services } }) => {
    await services.post.deletePost(postId);
    return { postId };
  }
);

const loadExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const reactPost = async ({ postId, services, getState, isLike = true }) => {
  const methodName = isLike ? 'likePost' : 'dislikePost';
  const propertyName = isLike ? 'likeCount' : 'dislikeCount';
  const otherPropertyName = isLike ? 'dislikeCount' : 'likeCount';

  const { updated: isUpdated, reaction: { id } } = await services.post[methodName](postId);
  const diff = id ? 1 : -1;

  const mapReactions = post => ({
    ...post,
    [propertyName]: Number(post[propertyName]) + diff,
    [otherPropertyName]: Number(post[otherPropertyName]) + (isUpdated ? -diff : 0)
  });

  const {
    posts: { posts, expandedPost }
  } = getState();

  const updated = posts.map(post => (post.id !== postId ? post : mapReactions(post)));
  const updatedExpandedPost = expandedPost?.id === postId ? mapReactions(expandedPost) : undefined;

  return { posts: updated, expandedPost: updatedExpandedPost };
};

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => reactPost({ postId, services, getState })
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => reactPost({ postId, services, getState, isLike: false })
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      ...(post.comments ? { comments: [...post.comments, comment] } : {})
    });

    const {
      posts: { posts, expandedPost }
    } = getState();

    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : expandedPost;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const updateComment = createAsyncThunk(
  ActionType.UPDATE_COMMENT,
  async ({ id: commentId, data }, { getState, extra: { services } }) => {
    const { id } = await services.comment.updateComment(commentId, data);
    const newComment = await services.comment.getComment(id);

    const mapComments = post => {
      const newComments = [...post.comments];
      const index = newComments.findIndex(c => c.id === id);

      if (index >= 0) {
        newComments[index] = newComment;
      }

      return {
        ...post,
        comments: newComments
      };
    };

    const {
      posts: { posts, expandedPost }
    } = getState();

    const updatedExpandedPost = expandedPost?.id === newComment.postId
      ? mapComments(expandedPost)
      : expandedPost;

    return { posts, expandedPost: updatedExpandedPost };
  }
);

const deleteComment = createAsyncThunk(
  ActionType.DELETE_COMMENT,
  async ({ id, postId }, { getState, extra: { services } }) => {
    await services.comment.deleteComment(id);

    const mapComments = post => {
      const newComments = post.comments ? [...post.comments] : null;

      if (newComments) {
        const index = newComments.findIndex(c => c.id === id);

        if (index >= 0) {
          newComments.splice(index, 1);
        }
      }

      return {
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments: newComments
      };
    };

    const {
      posts: { posts, expandedPost }
    } = getState();

    const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === postId
      ? mapComments(expandedPost)
      : expandedPost;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const reactComment = async ({ postId, commentId, services, getState, isLike = true }) => {
  const methodName = isLike ? 'likeComment' : 'dislikeComment';
  const propertyName = isLike ? 'likeCount' : 'dislikeCount';
  const otherPropertyName = isLike ? 'dislikeCount' : 'likeCount';

  const { updated: isUpdated, reaction: { id } } = await services.comment[methodName](commentId);
  const diff = id ? 1 : -1;

  const mapReactions = comment => ({
    ...comment,
    [propertyName]: Number(comment[propertyName]) + diff,
    [otherPropertyName]: Number(comment[otherPropertyName]) + (isUpdated ? -diff : 0)
  });

  const {
    posts: { expandedPost }
  } = getState();

  let newExpandedPost = expandedPost;

  if (expandedPost?.id === postId) {
    const updated = expandedPost.comments
      .map(comment => (comment.id !== commentId ? comment : mapReactions(comment)));

    newExpandedPost = {
      ...expandedPost,
      comments: updated
    };
  }

  return { expandedPost: newExpandedPost };
};

const likeComment = createAsyncThunk(
  ActionType.REACT_COMMENT,
  async ({ postId, commentId }, { getState, extra: { services } }) => reactComment({
    postId,
    commentId,
    services,
    getState
  })
);

const dislikeComment = createAsyncThunk(
  ActionType.REACT_COMMENT,
  async ({ postId, commentId }, { getState, extra: { services } }) => reactComment({
    postId,
    commentId,
    services,
    getState,
    isLike: false
  })
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  deletePost,
  loadExpandedPost,
  likePost,
  dislikePost,
  addComment,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment
};
