import { ExceptionMessage, ExceptionName } from '../../common/enums/enums';

class NotFoundError extends Error {
  constructor(entity) {
    super(`${entity}${ExceptionMessage.NOT_FOUND}`);
    this.name = ExceptionName.NOT_FOUND;
  }
}

export { NotFoundError };
