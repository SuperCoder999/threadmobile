import { PermissionsAndroid, Platform } from 'react-native';

export const isAndroidPermissionGranted = async permission => {
  if (Platform.OS !== 'android') {
    return true;
  }

  const wasGranted = await PermissionsAndroid.check(permission);

  if (wasGranted) {
    return true;
  }

  const status = await PermissionsAndroid.request(permission);
  return status === PermissionsAndroid.RESULTS.GRANTED;
};
