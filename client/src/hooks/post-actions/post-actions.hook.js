import { useCallback } from 'react';
import { threadActionCreator } from 'store/actions';

const usePostActions = dispatch => {
  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handlePostDelete = useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  return { handlePostLike, handlePostDislike, handlePostDelete };
};

export { usePostActions };
