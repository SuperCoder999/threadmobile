import * as React from 'react';
import {
  HomeScreenName,
  IconName,
  NotificationMessage,
  TextVariant
} from 'common/enums/enums';
import {
  Post,
  FlatList,
  Icon,
  Switch,
  Text,
  View
} from 'components/components';
import { AppColor } from 'config/config';
import { sharePost } from 'helpers/helpers';
import {
  useCallback,
  useDispatch,
  useEffect,
  useNavigation,
  useRef,
  useSelector,
  useState,
  usePostActions
} from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';
import { notification as notificationService } from 'services/services';
import styles from './styles';

const postsFilter = {
  userId: undefined,
  exceptUserId: undefined,
  likedByUserId: undefined,
  from: 0,
  count: 10
};

const ExpandedPost = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { posts, hasMorePosts, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    userId: state.profile.user?.id
  }));
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const isFlatListReady = useRef(false); // to avoid firing onEndReached on load on IOS
  const { handlePostLike, handlePostDislike, handlePostDelete } = usePostActions(dispatch);

  const handlePostExpand = useCallback(
    id => {
      dispatch(threadActionCreator.loadExpandedPost(id));
      navigation.navigate(HomeScreenName.EXPANDED_POST);
    },
    [dispatch, navigation]
  );

  const handlePostShare = useCallback(({ body, image }) => {
    sharePost({ body, image }).catch(() => {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    });
  }, []);

  const handlePostsLoad = useCallback(
    filtersPayload => dispatch(threadActionCreator.loadPosts(filtersPayload)),
    [dispatch]
  );

  const handleMorePostsLoad = filtersPayload => {
    setIsLoading(true);
    dispatch(threadActionCreator.loadMorePosts(filtersPayload))
      .unwrap()
      .finally(() => {
        setIsLoading(false);
      });
  };

  const getMorePosts = () => {
    if (isFlatListReady.current && hasMorePosts && !isLoading) {
      handleMorePostsLoad({ ...postsFilter });
      postsFilter.from += postsFilter.count;
    }
  };

  const reloadPosts = () => {
    postsFilter.from = 0;
    setIsLoading(true);

    const request = handlePostsLoad({ ...postsFilter });
    request.unwrap().finally(() => setIsLoading(false));

    postsFilter.from += postsFilter.count;
    return request;
  };

  const toggleShowOwnPosts = () => {
    if (!showOwnPosts) {
      postsFilter.exceptUserId = undefined;
      setShowNotOwnPosts(false);
    }

    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    reloadPosts();
  };

  const toggleShowNotOwnPosts = () => {
    if (!showNotOwnPosts) {
      postsFilter.userId = undefined;
      setShowOwnPosts(false);
    }

    setShowNotOwnPosts(!showNotOwnPosts);
    postsFilter.exceptUserId = showNotOwnPosts ? undefined : userId;
    reloadPosts();
  };

  const toggleShowLikedByMe = () => {
    setShowLikedByMe(!showLikedByMe);
    postsFilter.likedByUserId = showLikedByMe ? undefined : userId;
    reloadPosts();
  };

  const handleScroll = () => {
    isFlatListReady.current = true;
  };

  useEffect(() => {
    const request = reloadPosts();
    return () => request.abort();
  }, [handlePostsLoad, setIsLoading]);

  return (
    <FlatList
      bounces={false}
      data={posts}
      keyExtractor={({ id }) => id}
      ListHeaderComponent={(
        <>
          <View style={styles.header}>
            <Icon name={IconName.CAT} size={24} color={AppColor.HEADLINE} />
            <Text variant={TextVariant.HEADLINE} style={styles.logoText}>
              Thread
            </Text>
          </View>
          <View style={styles.filter}>
            <Switch
              value={showOwnPosts}
              label="Show only my posts"
              onToggleValue={toggleShowOwnPosts}
            />
            <Switch
              value={showNotOwnPosts}
              label="Show only other people's posts"
              onToggleValue={toggleShowNotOwnPosts}
            />
            <Switch
              value={showLikedByMe}
              label="Show only posts liked by me"
              onToggleValue={toggleShowLikedByMe}
            />
          </View>
        </>
      )}
      onEndReachedThreshold={0.1}
      onEndReached={getMorePosts}
      onScroll={handleScroll}
      renderItem={({ item: post }) => (
        <Post
          post={post}
          onPostLike={handlePostLike}
          onPostDislike={handlePostDislike}
          onPostShare={handlePostShare}
          onPostExpand={handlePostExpand}
          onPostDelete={handlePostDelete}
        />
      )}
    />
  );
};

export default ExpandedPost;
