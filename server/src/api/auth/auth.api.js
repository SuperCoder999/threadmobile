import { AuthApiPath } from '../../common/enums/enums';
import { catchErrors } from '../../helpers/helpers';

const initAuth = (router, opts, done) => {
  const { auth: authService, user: userService } = opts.services;

  // user added to the request (req.user) in auth plugin, authorization.plugin.js
  router
    .post(AuthApiPath.LOGIN, catchErrors(async req => {
      const user = await authService.verifyLoginCredentials(req.body);
      return authService.login(user);
    }))
    .post(AuthApiPath.REGISTER, catchErrors(req => authService.register(req.body)))
    .post(AuthApiPath.REQUEST_RESET, async req => {
      await authService.requestResetPassword(req.body.email);
      return {};
    })
    .post(AuthApiPath.RESET, catchErrors(async req => {
      await authService.resetPassword(req.body.token, req.body.password);
      return {};
    }))
    .post(AuthApiPath.CHECK_RESET, catchErrors(async req => {
      await authService.checkResetToken(req.body.token);
      return {};
    }))
    .get(AuthApiPath.USER, req => userService.getUserById(req.user.id));

  done();
};

export { initAuth };
