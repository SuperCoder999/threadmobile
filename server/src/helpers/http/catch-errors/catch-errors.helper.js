import { getErrorStatusCode } from '../get-status-code/get-status-code.helper';

const catchErrors = handler => async (req, res) => {
  try {
    return await handler(req, res);
  } catch (error) {
    return res.code(getErrorStatusCode(error)).send(error);
  }
};

export { catchErrors };
