import { verifyToken } from '../../token/token';
import { InvalidResetPasswordTokenError } from '../../../exceptions/exceptions';

const verifyResetToken = token => {
  try {
    return verifyToken(token);
  } catch {
    throw new InvalidResetPasswordTokenError();
  }
};

export { verifyResetToken };
