import { ENV } from '../../../common/enums/enums';

const getResetTokenExpiration = () => {
  const date = new Date();
  date.setMinutes(date.getMinutes() + ENV.RESET_PASSWORD.EXPIRES_IN_MIN);

  return date;
};

export { getResetTokenExpiration };
