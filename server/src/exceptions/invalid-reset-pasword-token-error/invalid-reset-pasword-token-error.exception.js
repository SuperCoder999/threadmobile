import { ExceptionMessage, ExceptionName } from '../../common/enums/enums';

class InvalidResetPasswordTokenError extends Error {
  constructor(message = ExceptionMessage.INVALID_RESET_PASSWORD_TOKEN) {
    super(message);
    this.name = ExceptionName.INVALID_RESET_PASSWORD_TOKEN;
  }
}

export { InvalidResetPasswordTokenError };
