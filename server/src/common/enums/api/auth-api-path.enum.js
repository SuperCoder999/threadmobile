const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  REQUEST_RESET: '/reset-password/request',
  RESET: '/reset-password',
  CHECK_RESET: '/reset-password/check'
};

export { AuthApiPath };
