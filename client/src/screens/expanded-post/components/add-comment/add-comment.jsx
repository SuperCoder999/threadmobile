import * as React from 'react';
import { TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { IconName } from 'common/enums/enums';
import { Icon, View } from 'components/components';
import { AppColor } from 'config/config';
import { useState } from 'hooks/hooks';
import { commentType } from 'common/prop-types/comment';
import styles from './styles';

const AddComment = ({ postId, defaultComment, onCommentSubmit }) => {
  const [body, setBody] = useState(defaultComment?.body ?? '');

  const handleAddComment = async () => {
    if (!body) {
      return;
    }

    await onCommentSubmit({ body, postId });
    setBody('');
  };

  return (
    <View style={styles.container}>
      <TextInput
        value={body}
        placeholder="Type your comment..."
        placeholderTextColor={AppColor.PLACEHOLDER}
        style={styles.input}
        onChangeText={setBody}
      />
      <Icon
        name={IconName.PAPER_PLANE}
        size={22}
        color={body ? AppColor.PRIMARY : AppColor.ICON}
        onPress={body ? handleAddComment : null}
      />
    </View>
  );
};

AddComment.propTypes = {
  postId: PropTypes.number.isRequired,
  onCommentSubmit: PropTypes.func.isRequired,
  defaultComment: commentType
};

AddComment.defaultProps = {
  defaultComment: null
};

export default AddComment;
