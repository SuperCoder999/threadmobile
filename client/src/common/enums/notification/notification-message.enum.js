const NotificationMessage = {
  OPERATION_FAILED: 'Something went wrong.',
  LIKE: 'Your post was liked!',
  IMAGE_DOWNLOAD: 'Image was downloaded successfully!',
  INVALID_RESET_PASSWORD_TOKEN: 'Invalid or expired reset password token.'
};

export { NotificationMessage };
