import { createWorker } from 'celery-node';
import { registerWorkerTasks } from './celery/tasks/tasks';
import { ENV } from './common/enums/app/env.enum';

const worker = createWorker(ENV.CELERY.BROKER, ENV.CELERY.BACKEND);
registerWorkerTasks(worker);

worker.start();
