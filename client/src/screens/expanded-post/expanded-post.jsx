import * as React from 'react';
import { NotificationMessage } from 'common/enums/enums';
import { Post, FlatList, Spinner } from 'components/components';
import { sharePost } from 'helpers/helpers';
import { useCallback, useEffect, useDispatch, useSelector, usePostActions, useRoute } from 'hooks/hooks';
import { notification as notificationService } from 'services/services';
import { threadActionCreator } from 'store/actions';
import { AddComment, Comment } from './components/components';
import { getSortedComments } from './helpers/helpers';
import styles from './styles';

const ExpandedPost = () => {
  const dispatch = useDispatch();

  const { params } = useRoute();
  const routePostId = params?.postId;

  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const sortedComments = getSortedComments(post?.comments ?? []);
  const { handlePostLike, handlePostDislike, handlePostDelete } = usePostActions(dispatch);

  const handleCommentAdd = useCallback(
    commentPayload => dispatch(threadActionCreator.addComment(commentPayload)),
    [dispatch]
  );

  const handlePostShare = useCallback(
    ({ body, image }) => sharePost({ body, image }).catch(() => {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    }),
    []
  );

  const handleCommentLike = useCallback(
    commentId => (post ? dispatch(threadActionCreator.likeComment({ postId: post.id, commentId })) : null),
    [post, dispatch]
  );

  const handleCommentDislike = useCallback(
    commentId => (post ? dispatch(threadActionCreator.dislikeComment({ postId: post.id, commentId })) : null),
    [post, dispatch]
  );

  const handleCommentUpdate = useCallback(
    (id, data) => dispatch(threadActionCreator.updateComment({ id, data })),
    [dispatch]
  );

  const handleCommentDelete = useCallback(
    id => (post ? dispatch(threadActionCreator.deleteComment({ id, postId: post.id })) : null),
    [post, dispatch]
  );

  useEffect(() => {
    if (routePostId) {
      dispatch(threadActionCreator.loadExpandedPost(routePostId));
    }
  }, []);

  if (!post) {
    return <Spinner isOverflow />;
  }

  return (
    <FlatList
      bounces={false}
      data={sortedComments}
      keyExtractor={({ id }) => id}
      ListHeaderComponentStyle={styles.header}
      contentContainerStyle={styles.container}
      ListHeaderComponent={(
        <>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostShare={handlePostShare}
            onPostDelete={handlePostDelete}
          />
          <AddComment postId={post.id} onCommentSubmit={handleCommentAdd} />
        </>
      )}
      renderItem={({ item: comment }) => (
        <Comment
          comment={comment}
          onCommentLike={handleCommentLike}
          onCommentDislike={handleCommentDislike}
          onCommentUpdate={handleCommentUpdate}
          onCommentDelete={handleCommentDelete}
        />
      )}
    />
  );
};

export default ExpandedPost;
