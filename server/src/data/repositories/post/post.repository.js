import { Abstract } from '../abstract/abstract.repository';
import {
  getCommentsCountQuery,
  getReactionsQuery,
  getWhereQuery,
  getCommentReactionsQuery
} from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      exceptUserId,
      likedByUserId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model, true),
        getReactionsQuery(this.model, false)
      )
      .where(getWhereQuery({ userId, exceptUserId, likedByUserId }))
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model, true),
        getReactionsQuery(this.model, false)
      )
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .modifyGraph('comments', query => query.select(
        'comments.*',
        getCommentReactionsQuery(true),
        getCommentReactionsQuery(false)
      ))
      .first();
  }

  async getPostUserId(id) {
    const { userId } = await this.model.query()
      .select('posts.userId')
      .where({ id })
      .first();

    return userId;
  }
}

export { Post };
