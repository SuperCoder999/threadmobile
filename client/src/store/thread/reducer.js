import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';
import * as profileActions from '../profile/actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(threadActions.updatePost.fulfilled, (state, action) => {
    const { post } = action.payload;
    const newPosts = [...state.posts];

    const index = newPosts.findIndex(p => p.id === post.id);
    newPosts[index] = post;

    state.posts = newPosts;

    if (post.id === state.expandedPost?.id) {
      state.expandedPost = post;
    }
  });
  builder.addCase(threadActions.deletePost.fulfilled, (state, action) => {
    const { postId } = action.payload;
    const newPosts = [...state.posts];

    const index = newPosts.findIndex(p => p.id === postId);
    newPosts.splice(index, 1);

    state.posts = newPosts;

    if (postId === state.expandedPost?.id) {
      state.expandedPost = null;
    }
  });
  builder.addCase(profileActions.update.fulfilled, (state, action) => {
    const { id, image, imageId, username, status } = action.payload;
    const user = { id, image, imageId, username, status };

    state.posts = state.posts.map(post => {
      if (post.userId === user.id) {
        return { ...post, user };
      }

      return post;
    });

    if (state.expandedPost?.userId === user.id) {
      state.expandedPost = { ...state.expandedPost, user };
    }
  });
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.fulfilled,
      threadActions.dislikePost.fulfilled,
      threadActions.addComment.fulfilled,
      threadActions.updateComment.fulfilled,
      threadActions.deleteComment.fulfilled
    ),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.likeComment.fulfilled,
      threadActions.dislikeComment.fulfilled
    ),
    (state, action) => {
      const { expandedPost } = action.payload;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.applyPost.fulfilled,
      threadActions.createPost.fulfilled
    ),
    (state, action) => {
      const { post } = action.payload;

      state.posts = [post, ...state.posts];
    }
  );
});

export { reducer };
