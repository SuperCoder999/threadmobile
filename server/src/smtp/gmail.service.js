import { google } from 'googleapis';
import { ENV } from '../common/enums/enums';
import { SMTP } from './abstract/smtp.service';

class GMail extends SMTP {
  constructor() {
    const oauth = new google.auth.OAuth2(
      ENV.GOOGLE.GOOGLE_CLIENT_ID,
      ENV.GOOGLE.GOOGLE_CLIENT_SECRET,
      'https://developers.google.com/oauthplayground'
    );

    oauth.setCredentials({
      refresh_token: ENV.GOOGLE.GOOGLE_REFRESH_TOKEN
    });

    const accessToken = oauth.getAccessToken();

    super(ENV.GOOGLE.GMAIL_USERNAME, ENV.GOOGLE.GMAIL_MASK, {
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: ENV.GOOGLE.GMAIL_USERNAME,
        clientId: ENV.GOOGLE.GOOGLE_CLIENT_ID,
        clientSecret: ENV.GOOGLE.GOOGLE_CLIENT_SECRET,
        refreshToken: ENV.GOOGLE.GOOGLE_REFRESH_TOKEN,
        accessToken
      }
    });
  }
}

export { GMail };
