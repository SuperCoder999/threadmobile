import {
  UserPayloadKey,
  UserValidationMessage,
  UserValidationRule
} from 'common/enums/enums';
import { Joi } from 'helpers/helpers';

const profileUpdate = Joi.object({
  [UserPayloadKey.USERNAME]: Joi.string()
    .trim()
    .min(UserValidationRule.USERNAME_MIN_LENGTH)
    .max(UserValidationRule.USERNAME_MAX_LENGTH)
    .required()
    .messages({
      'string.empty': UserValidationMessage.USERNAME_REQUIRE,
      'string.min': UserValidationMessage.USERNAME_MIN_LENGTH,
      'string.max': UserValidationMessage.USERNAME_MAX_LENGTH
    }),
  [UserPayloadKey.STATUS]: Joi.string()
    .trim()
    .max(UserValidationRule.STATUS_MAX_LENGTH)
    .messages({
      'string.max': UserValidationMessage.STATUS_MAX_LENGTH
    }),
  [UserPayloadKey.EMAIL]: Joi.string()
    .trim()
    .email({ tlds: { allow: false } })
    .required()
    .messages({
      'string.email': UserValidationMessage.EMAIL_WRONG,
      'string.empty': UserValidationMessage.EMAIL_REQUIRE
    })
});

export { profileUpdate };
