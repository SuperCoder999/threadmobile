import { PermissionsAndroid } from 'react-native';
import RNFetchBlob from 'react-native-blob-util';
import CameraRoll from '@react-native-community/cameraroll';
import { isAndroidPermissionGranted } from 'helpers/helpers';
import { notification as notificationService } from 'services/services';
import { NotificationMessage } from 'common/enums/enums';

export const downloadImage = async imageUri => {
  if (isAndroidPermissionGranted(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)) {
    const urlFilename = imageUri.split('/').pop();
    const extension = urlFilename.split('.').pop();

    await RNFetchBlob
      .config({
        fileCache: true,
        appendExt: extension
      })
      .fetch('GET', imageUri)
      .then(res => CameraRoll.save(res.data, 'photo')
        .then(() => notificationService.info(NotificationMessage.IMAGE_DOWNLOAD)));
  }
};
