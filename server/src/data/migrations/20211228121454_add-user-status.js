const TableName = {
  USERS: 'users'
};

const ColumnName = {
  STATUS: 'status'
};

export async function up(knex) {
  await knex.schema.alterTable(TableName.USERS, table => {
    table.string(ColumnName.STATUS);
  });
}

export async function down(knex) {
  await knex.schema.alterTable(TableName.USERS, table => {
    table.dropColumn(ColumnName.STATUS);
  });
}
