import * as React from 'react';
import {
  UserPayloadKey,
  IconName,
  TextVariant,
  AuthScreenName,
  AuthFormType,
  NotificationMessage
} from 'common/enums/enums';
import {
  useState,
  useEffect,
  useAppForm,
  useRoute,
  useDispatch,
  useNavigation,
  useCallback
} from 'hooks/hooks';
import { View, Stack, Input, Button, Text, InfoMessage } from 'components/components';
import { resetPassword as resetPasswordValidationSchema } from 'validation-schemas/validation-schemas';
import { profileActionCreator } from 'store/actions';
import { notification as notificationService } from 'services/services';
import { DEFAULT_RESET_PAYLOAD } from './constants';
import styles from './styles';

const ResetPassword = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const { params } = useRoute();
  const token = params?.token;

  const [isLoading, setIsLoading] = useState(true);

  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_RESET_PAYLOAD,
    validationSchema: resetPasswordValidationSchema
  });

  const redirectToLogin = useCallback(() => navigation.navigate(
    AuthScreenName.LOGIN_REGISTER,
    { formType: AuthFormType.LOGIN }
  ), [navigation]);

  useEffect(() => {
    const request = dispatch(profileActionCreator.checkResetPasswordToken(token));

    request
      .unwrap()
      .then(() => setIsLoading(false))
      .catch(() => {
        redirectToLogin();
        notificationService.error(NotificationMessage.INVALID_RESET_PASSWORD_TOKEN);
      });

    return () => request.abort();
  }, [token, redirectToLogin]);

  const handleResetPassword = values => {
    if (errors.length) {
      return;
    }

    setIsLoading(true);

    dispatch(profileActionCreator.resetPassword({ token, password: values.password }))
      .unwrap()
      .then(redirectToLogin)
      .finally(() => setIsLoading(false));
  };

  const handleLoginPress = () => {
    navigation.navigate(AuthScreenName.LOGIN_REGISTER, { formType: AuthFormType.LOGIN });
  };

  if (!token) {
    handleLoginPress();
    return null;
  }

  return (
    <View style={styles.screen}>
      <Text style={styles.header} variant={TextVariant.HEADLINE}>
        Reset password
      </Text>
      <Stack space={15}>
        <Input
          name={UserPayloadKey.PASSWORD}
          control={control}
          errors={errors}
          placeholder="Enter new strong password..."
          iconName={IconName.LOCK}
          isDisabled={isLoading}
          isSecure
        />
        <Button
          title="Reset password"
          isLoading={isLoading}
          onPress={handleSubmit(handleResetPassword)}
        />
      </Stack>
      <InfoMessage>
        <Text variant={TextVariant.LINK} onPress={handleLoginPress}>
          Return to login
        </Text>
      </InfoMessage>
    </View>
  );
};

export default ResetPassword;
