import { ProfileApiPath } from '../../common/enums/enums';

const initProfile = (router, opts, done) => {
  const { user: userService } = opts.services;

  router
    .patch(ProfileApiPath.UPDATE, req => userService.update(req.user.id, req.body));

  done();
};

export { initProfile };
