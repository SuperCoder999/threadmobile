import { DeepLinksApiPath, HttpCode } from '../../common/enums/enums';

const initDeepLinks = (router, _, done) => {
  router.get(DeepLinksApiPath.ROOT, (req, res) => {
    const redirectLink = Buffer.from(req.query.link, 'base64').toString('utf8');
    res.status(HttpCode.MOVED_PERMANENTLY).redirect(redirectLink);
  });

  done();
};

export { initDeepLinks };
