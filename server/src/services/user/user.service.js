class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  getUserById(id) {
    return this._userRepository.getUserById(id);
  }

  async update(userId, data) {
    await this._userRepository.updateById(userId, data);
    return this.getUserById(userId);
  }
}

export { User };
