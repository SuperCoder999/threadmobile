const ExceptionName = {
  INVALID_CREDENTIALS: 'InvalidCredentials.',
  NOT_POST_AUTHOR: 'NotPostAuthor.',
  INVALID_RESET_PASSWORD_TOKEN: 'InvalidResetPasswordToken.',
  NOT_FOUND: 'NotFound.'
};

export { ExceptionName };
