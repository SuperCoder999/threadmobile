const LinkingRoutes = {
  LIKED_POST: 'liked-post/:postId',
  RESET_PASSWORD: 'reset-password/:token'
};

export { LinkingRoutes };
