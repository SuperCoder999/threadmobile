import { Abstract } from '../abstract/abstract.repository';
import { getReactionsQuery } from './helpers';

class Comment extends Abstract {
  constructor({ commentModel }) {
    super(commentModel);
  }

  getCommentById(id) {
    return this.model.query()
      .select(
        'comments.*',
        getReactionsQuery(this.model, true),
        getReactionsQuery(this.model, false)
      )
      .where({ id })
      .withGraphFetched('[user.image]')
      .first();
  }
}

export { Comment };
