import { CommentModel, PostReactionModel } from '../../models';

const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = (model, isLike) => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getWhereUserIdQuery = (userId, builder) => {
  if (userId) {
    builder.where({ userId });
  }
};

const getWhereExceptUserIdQuery = (userId, exceptUserId, builder) => {
  if (exceptUserId && !userId) {
    builder.whereNot({ userId: exceptUserId });
  }
};

const getWhereLikedByUserIdQuery = (likedByUserId, builder) => {
  if (likedByUserId) {
    builder.whereExists(
      PostReactionModel.query()
        .select(1)
        .where({ userId: likedByUserId, isLike: true })
        .whereColumn('postReactions.postId', 'posts.id')
    );
  }
};

const getWhereQuery = ({ userId, exceptUserId, likedByUserId }) => builder => {
  getWhereUserIdQuery(userId, builder);
  getWhereExceptUserIdQuery(userId, exceptUserId, builder);
  getWhereLikedByUserIdQuery(likedByUserId, builder);
};

const getCommentReactionsQuery = isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return CommentModel.relatedQuery('commentReactions')
    .count()
    .where({ isLike })
    .as(col);
};

export { getCommentsCountQuery, getReactionsQuery, getWhereQuery, getCommentReactionsQuery };
