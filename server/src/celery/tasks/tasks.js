import * as sendEmailTask from './send-email-template/send-email-template.task';

const tasks = [sendEmailTask];

const registerWorkerTasks = worker => {
  tasks.forEach(({ name, func }) => worker.register(name, func));
};

const registerClientTasks = client => {
  const taskEntries = tasks.map(({ name }) => [name, client.createTask(name)]);
  const tasksAsObject = Object.fromEntries(taskEntries);

  return tasksAsObject;
};

export { registerWorkerTasks, registerClientTasks };
