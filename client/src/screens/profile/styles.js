import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
    padding: 15
  },
  content: {
    minWidth: 240
  },
  avatar: {
    marginBottom: 10,
    width: 250,
    height: 250,
    borderRadius: 125
  }
});

export default styles;
