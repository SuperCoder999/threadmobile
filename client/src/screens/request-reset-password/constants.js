import { UserPayloadKey } from 'common/enums/enums';

const DEFAULT_REQUEST_RESET_PAYLOAD = {
  [UserPayloadKey.EMAIL]: ''
};

export { DEFAULT_REQUEST_RESET_PAYLOAD };
