import * as React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import styles from './styles';

const InfoMessage = ({ children }) => <View style={styles.message}>{children}</View>;

InfoMessage.propTypes = {
  children: PropTypes.node
};

InfoMessage.defaultProps = {
  children: null
};

export default InfoMessage;
