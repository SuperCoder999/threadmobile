import { ExceptionMessage, ExceptionName } from '../../common/enums/enums';

class NotPostAuthorError extends Error {
  constructor(message = ExceptionMessage.NOT_POST_AUTHOR) {
    super(message);
    this.name = ExceptionName.NOT_POST_AUTHOR;
  }
}

export { NotPostAuthorError };
