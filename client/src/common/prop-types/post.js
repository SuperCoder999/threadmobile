import PropTypes from 'prop-types';
import { imageType } from 'common/prop-types/image';
import { commentType } from 'common/prop-types/comment';
import { shortUserType } from 'common/prop-types/user';

const postType = PropTypes.exact({
  id: PropTypes.number.isRequired,
  body: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  image: imageType,
  imageId: PropTypes.number,
  likeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  dislikeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  commentCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  comments: PropTypes.arrayOf(commentType),
  userId: PropTypes.number.isRequired,
  user: shortUserType.isRequired
});

export { postType };
