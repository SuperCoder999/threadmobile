import { AppScreenName, AuthScreenName, HomeScreenName, LinkingRoutes, RootScreenName } from 'common/enums/enums';

const LinkingSettings = {
  prefixes: ['thread://'],
  config: {
    screens: {
      [RootScreenName.APP]: {
        screens: {
          [AppScreenName.HOME]: {
            screens: {
              [HomeScreenName.EXPANDED_POST]: LinkingRoutes.LIKED_POST
            }
          }
        }
      },
      [RootScreenName.AUTH]: {
        screens: {
          [AuthScreenName.RESET]: LinkingRoutes.RESET_PASSWORD
        }
      }
    }
  }
};

export { LinkingSettings };
