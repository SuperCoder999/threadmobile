const TableName = {
  USERS: 'users'
};

const ColumnName = {
  RESET_PASSWORD_TOKEN: 'resetPasswordToken',
  RESET_PASSWORD_TOKEN_EXPIRES: 'resetPasswordTokenExpires'
};

export async function up(knex) {
  await knex.schema.alterTable(TableName.USERS, table => {
    table.string(ColumnName.RESET_PASSWORD_TOKEN);
    table.string(ColumnName.RESET_PASSWORD_TOKEN_EXPIRES);
  });
}

export async function down(knex) {
  await knex.schema.alterTable(TableName.USERS, table => {
    table.dropColumn(ColumnName.RESET_PASSWORD_TOKEN);
    table.dropColumn(ColumnName.RESET_PASSWORD_TOKEN_EXPIRES);
  });
}
