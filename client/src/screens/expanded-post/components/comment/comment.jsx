import * as React from 'react';
import PropTypes from 'prop-types';
import { TextVariant, IconName } from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { commentType } from 'common/prop-types/prop-types';
import { Image, Stack, Text, View, Icon } from 'components/components';
import { getFromNowTime } from 'helpers/helpers';
import { useState, useSelector } from 'hooks/hooks';
import AddComment from '../add-comment/add-comment';
import styles from './styles';

const Comment = ({ onCommentLike, onCommentDislike, onCommentUpdate, onCommentDelete, comment }) => {
  const {
    id,
    likeCount,
    dislikeCount,
    body,
    user,
    postId,
    createdAt
  } = comment;

  const { user: currentUser } = useSelector(state => state.profile);
  const isOwnComment = currentUser?.id === user.id;
  const date = getFromNowTime(createdAt);

  const [isUpdating, setIsUpdating] = useState(false);

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleCommentDelete = () => onCommentDelete(id);
  const toggleCommentUpdate = () => setIsUpdating(!isUpdating);

  const handleCommentUpdate = data => {
    toggleCommentUpdate();
    onCommentUpdate(id, data);
  };

  return (
    <>
      <View style={styles.container}>
        <Image
          style={styles.avatar}
          accessibilityIgnoresInvertColors
          source={{ uri: user.image?.link ?? DEFAULT_USER_AVATAR }}
        />
        <View style={styles.content}>
          <View style={styles.header}>
            <Text variant={TextVariant.TITLE}>{user.username}</Text>
            <Text variant={TextVariant.SUBTITLE}>
              {' • '}
              {date}
            </Text>
          </View>
          {user.status ? (
            <Text variant={TextVariant.SUBTITLE}>
              {user.status}
            </Text>
          ) : null}
          <Text style={styles.body}>{body}</Text>
          <Stack space={24} isRow>
            <Icon
              name={IconName.THUMBS_UP}
              size={16}
              label={String(likeCount)}
              onPress={handleCommentLike}
            />
            <Icon
              name={IconName.THUMBS_DOWN}
              size={16}
              label={String(dislikeCount)}
              onPress={handleCommentDislike}
            />
            {isOwnComment ? (
              <Icon
                name={IconName.EDIT}
                size={16}
                onPress={toggleCommentUpdate}
              />
            ) : null}
            {isOwnComment ? (
              <Icon
                name={IconName.TRASH}
                size={16}
                onPress={handleCommentDelete}
              />
            ) : null}
          </Stack>
        </View>
      </View>
      {isOwnComment && isUpdating ? (
        <AddComment
          postId={postId}
          defaultComment={comment}
          onCommentSubmit={handleCommentUpdate}
        />
      ) : null}
    </>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
