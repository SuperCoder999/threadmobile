import * as React from 'react';
import { UserPayloadKey, IconName, TextVariant, AuthScreenName, AuthFormType } from 'common/enums/enums';
import { View, Stack, Input, Button, Text, InfoMessage } from 'components/components';
import { useState, useDispatch, useAppForm, useNavigation } from 'hooks/hooks';
import { requestResetPassword as requestResetPasswordValidationSchema } from 'validation-schemas/validation-schemas';
import { profileActionCreator } from 'store/actions';
import { DEFAULT_REQUEST_RESET_PAYLOAD } from './constants';
import styles from './styles';

const RequestResetPassword = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState(false);
  const [isRequested, setIsRequested] = useState(false);

  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_REQUEST_RESET_PAYLOAD,
    validationSchema: requestResetPasswordValidationSchema
  });

  const handleRequestResetPassword = ({ email }) => {
    if (errors.length) {
      return;
    }

    setIsLoading(true);

    dispatch(profileActionCreator.requestResetPassword(email))
      .unwrap()
      .then(() => setIsRequested(true))
      .finally(() => setIsLoading(false));
  };

  const handleLoginPress = () => {
    navigation.navigate(AuthScreenName.LOGIN_REGISTER, { formType: AuthFormType.LOGIN });
  };

  const handleRerequestPress = () => {
    navigation.navigate(AuthScreenName.REQUEST_RESET);
  };

  return (
    <View style={styles.screen}>
      <Text style={styles.header} variant={TextVariant.HEADLINE}>
        {isRequested
          ? 'Reset password link has been sent to your email'
          : 'Reset password'}
      </Text>
      {isRequested ? null : (
        <Stack space={15}>
          <Input
            name={UserPayloadKey.EMAIL}
            control={control}
            errors={errors}
            placeholder="johndoe@mail.com"
            iconName={IconName.ENVELOPE}
            isDisabled={isLoading}
          />
          <Button
            title="Reset password"
            isLoading={isLoading}
            onPress={handleSubmit(handleRequestResetPassword)}
          />
        </Stack>
      )}
      <InfoMessage>
        <Text variant={TextVariant.LINK} onPress={handleLoginPress}>
          Return to login
        </Text>
      </InfoMessage>
      {isRequested ? (
        <InfoMessage>
          <Text variant={TextVariant.LINK} onPress={handleRerequestPress}>
            Re-request password reset
          </Text>
        </InfoMessage>
      ) : null}
    </View>
  );
};

export default RequestResetPassword;
