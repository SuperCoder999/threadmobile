import * as React from 'react';
import PropTypes from 'prop-types';
import ModifyPostForm from 'components/modify-post-form/modify-post-form';
import { postType } from 'common/prop-types/prop-types';
import { useDispatch } from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';

const UpdatePost = ({ defaultPost, onSubmit }) => {
  const dispatch = useDispatch();

  const handleUpdatePost = payload => {
    dispatch(threadActionCreator.updatePost({ id: defaultPost.id, ...payload }));

    if (onSubmit) {
      onSubmit();
    }
  };

  return <ModifyPostForm defaultPost={defaultPost} onSubmit={handleUpdatePost} />;
};

UpdatePost.propTypes = {
  defaultPost: postType.isRequired,
  onSubmit: PropTypes.func
};

UpdatePost.defaultProps = {
  onSubmit: null
};

export default UpdatePost;
