const UserValidationRule = {
  USERNAME_MIN_LENGTH: 2,
  USERNAME_MAX_LENGTH: 30,
  STATUS_MAX_LENGTH: 70,
  PASSWORD_MIN_LENGTH: 3,
  PASSWORD_MAX_LENGTH: 30
};

export { UserValidationRule };
