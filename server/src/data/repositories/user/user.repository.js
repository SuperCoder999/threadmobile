import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor({ userModel }) {
    super(userModel);
  }

  getByEmail(email) {
    return this.model.query().select().where({ email }).first();
  }

  getByUsername(username) {
    return this.model.query().select().where({ username }).first();
  }

  getUserById(id) {
    return this.model.query()
      .select('id', 'createdAt', 'email', 'updatedAt', 'username', 'status')
      .where({ id })
      .withGraphFetched('[image]')
      .first();
  }
}

export { User };
