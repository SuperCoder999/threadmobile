const AuthScreenName = {
  LOGIN_REGISTER: 'Login Register',
  REQUEST_RESET: 'Request Reset',
  RESET: 'Reset'
};

export { AuthScreenName };
