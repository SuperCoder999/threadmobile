import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { IconName, TextVariant } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { Icon, Image, Stack, Text, View } from 'components/components';
import UpdatePost from 'components/update-post/update-post';
import { downloadImage, getFromNowTime } from 'helpers/helpers';
import { useSelector, useState } from 'hooks/hooks';
import styles from './styles';

const Post = ({ post, onPostLike, onPostDislike, onPostShare, onPostExpand, onPostDelete }) => {
  const [isUpdateOpen, setIsUpdateOpen] = useState(false);

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const currentUserId = useSelector(state => state.profile.user?.id);
  const date = getFromNowTime(createdAt);
  const isOwnPost = user.id === currentUserId;

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostExpand = () => onPostExpand(id);
  const handlePostShare = () => onPostShare({ body, image });
  const handlePostDelete = () => onPostDelete(id);
  const handleImageDownload = () => image && downloadImage(image.link);
  const toggleUpdate = () => setIsUpdateOpen(!isUpdateOpen);

  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        accessibilityIgnoresInvertColors
        source={{ uri: user.image?.link ?? DEFAULT_USER_AVATAR }}
      />
      <View style={styles.content}>
        <View style={styles.header}>
          <Text variant={TextVariant.TITLE}>{user.username}</Text>
          <Text variant={TextVariant.SUBTITLE}>
            {' • '}
            {date}
          </Text>
        </View>
        {user.status ? (
          <Text variant={TextVariant.SUBTITLE}>
            {user.status}
          </Text>
        ) : null}
        {image && (
          <View style={styles.imageSlot}>
            <Image
              style={styles.image}
              accessibilityIgnoresInvertColors
              source={{ uri: image.link }}
            />
            <TouchableOpacity style={styles.imageButton} onPress={handleImageDownload}>
              <Icon
                name={IconName.DOWNLOAD}
                size={16}
              />
            </TouchableOpacity>
          </View>
        )}
        <Text style={styles.body}>{body}</Text>
        <View style={styles.footer}>
          <Stack space={24} isRow>
            <Icon
              name={IconName.THUMBS_UP}
              size={16}
              label={String(likeCount)}
              onPress={handlePostLike}
            />
            <Icon
              name={IconName.THUMBS_DOWN}
              size={16}
              label={String(dislikeCount)}
              onPress={handlePostDislike}
            />
            <Icon
              name={IconName.COMMENT}
              size={16}
              label={String(commentCount)}
              onPress={onPostExpand ? handlePostExpand : null}
            />
            <Icon name={IconName.SHARE_ALT} size={16} onPress={handlePostShare} />
            {isOwnPost ? (
              <Icon
                name={IconName.EDIT}
                size={16}
                onPress={toggleUpdate}
              />
            ) : null}
            {isOwnPost ? (
              <Icon
                name={IconName.TRASH}
                size={16}
                onPress={handlePostDelete}
              />
            ) : null}
          </Stack>
        </View>
        {isUpdateOpen && isOwnPost ? <UpdatePost defaultPost={post} onSubmit={toggleUpdate} /> : null}
      </View>
    </View>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostShare: PropTypes.func.isRequired,
  onPostExpand: PropTypes.func,
  onPostDelete: PropTypes.func.isRequired
};

Post.defaultProps = {
  onPostExpand: null
};

export default Post;
