const generateRandomString = length => {
  const charset = '0123456789abcdefghijklmnopqrstuvwxzSBCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+-=?.,';
  let string = '';

  for (let i = 0; i < length; i++) {
    const randomCharIndex = Math.floor(Math.random() * charset.length);
    const char = charset[randomCharIndex];

    string += char;
  }

  return string;
};

export { generateRandomString };
