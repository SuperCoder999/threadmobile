import * as React from 'react';
import PropTypes from 'prop-types';
import {
  AuthFormType,
  AuthScreenName,
  IconName,
  NotificationMessage,
  TextVariant,
  UserPayloadKey
} from 'common/enums/enums';
import { Button, InfoMessage, Input, Stack, Text, View } from 'components/components';
import { useAppForm, useNavigation, useState } from 'hooks/hooks';
import { login as loginValidationSchema } from 'validation-schemas/validation-schemas';
import { notification as notificationService } from 'services/services';
import { DEFAULT_LOGIN_PAYLOAD } from './constants';

const LoginForm = ({ onLogin }) => {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_LOGIN_PAYLOAD,
    validationSchema: loginValidationSchema
  });

  const handleLogin = values => {
    setIsLoading(true);

    onLogin(values)
      .unwrap()
      .catch(() => {
        notificationService.error(NotificationMessage.OPERATION_FAILED);
        setIsLoading(false);
      });
  };

  const handleSignUpPress = () => {
    navigation.setParams({
      formType: AuthFormType.REGISTRATION
    });
  };

  const handleForgotPasswordPress = () => {
    navigation.navigate(AuthScreenName.REQUEST_RESET);
  };

  return (
    <View>
      <Stack space={15}>
        <Input
          name={UserPayloadKey.EMAIL}
          control={control}
          errors={errors}
          placeholder="johndoe@mail.com"
          iconName={IconName.ENVELOPE}
          isDisabled={isLoading}
        />
        <Input
          name={UserPayloadKey.PASSWORD}
          control={control}
          errors={errors}
          placeholder="password"
          iconName={IconName.LOCK}
          isDisabled={isLoading}
          isSecure
        />
        <Button
          title="Login"
          isLoading={isLoading}
          onPress={handleSubmit(handleLogin)}
        />
      </Stack>
      <InfoMessage>
        <Text>Don’t have an account? </Text>
        <Text variant={TextVariant.LINK} onPress={handleSignUpPress}>
          Sign up
        </Text>
      </InfoMessage>
      <InfoMessage>
        <Text>Forgot password? </Text>
        <Text variant={TextVariant.LINK} onPress={handleForgotPasswordPress}>
          Reset it!
        </Text>
      </InfoMessage>
    </View>
  );
};

LoginForm.propTypes = {
  onLogin: PropTypes.func.isRequired
};

export default LoginForm;
