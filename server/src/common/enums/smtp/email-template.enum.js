const EmailTemplate = {
  LIKE: {
    subject: 'Your post was liked!',
    body: `<h3>Hello, {{ username }}</h3>
<p>
  Your post was liked by {{ likerUsername }}!
</p>
<p>
  See the post here: <a href="{{ link }}">{{ link }}</a>
</p>`
  },
  RESET_PASSWORD: {
    subject: 'ThreadMobile Password Reset',
    body: `<h3>Hello, {{ username }}</h3>
<p>
  A password reset was requested for your ThreadMobile account.
  </br>
  To open ThreadMobile app and reset the password, please follow this link: <a href="{{ link }}">{{ link }}</a>
</p>
<h4>If you didn't request password reset, just ignore this email.</h4>`
  }
};

export { EmailTemplate };
