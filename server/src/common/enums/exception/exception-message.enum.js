const ExceptionMessage = {
  EMAIL_ALREADY_EXISTS: 'Email is already taken.',
  INCORRECT_EMAIL: 'Incorrect email.',
  INVALID_TOKEN: 'Token is invalid.',
  PASSWORDS_NOT_MATCH: 'Passwords do not match.',
  USERNAME_ALREADY_EXISTS: 'Username is already taken.',
  NOT_POST_AUTHOR: "You're not this post's author.",
  INVALID_RESET_PASSWORD_TOKEN: 'Invalid or expired reset password token.',
  NOT_FOUND: ' is not found.'
};

export { ExceptionMessage };
