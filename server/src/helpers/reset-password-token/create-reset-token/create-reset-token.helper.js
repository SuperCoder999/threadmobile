import { generateRandomString } from '../../random-string/random-string';
import { createToken } from '../../token/create-token/create-token.helper';

const createResetToken = userId => {
  const salt = generateRandomString(16);
  const data = { id: userId, salt };

  return { token: createToken(data), salt };
};

export { createResetToken };
