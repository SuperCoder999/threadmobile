const TableName = {
  USERS: 'users',
  COMMENTS: 'comments',
  COMMENT_REACTIONS: 'commentReactions'
};

const ColumnName = {
  ID: 'id',
  IS_LIKE: 'isLike',
  COMMENT_ID: 'commentId',
  USER_ID: 'userId',
  CREATED_AT: 'createdAt',
  UPDATED_AT: 'updatedAt'
};

const RelationRule = {
  CASCADE: 'CASCADE',
  SET_NULL: 'SET NULL'
};

export async function up(knex) {
  await knex.schema.createTable(TableName.COMMENT_REACTIONS, table => {
    table.increments(ColumnName.ID).primary();
    table.boolean(ColumnName.IS_LIKE).notNullable().defaultTo(true);
    table.dateTime(ColumnName.CREATED_AT).notNullable().defaultTo(knex.fn.now());
    table.dateTime(ColumnName.UPDATED_AT).notNullable().defaultTo(knex.fn.now());

    table
      .integer(ColumnName.USER_ID)
      .references(ColumnName.ID)
      .inTable(TableName.USERS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
    table
      .integer(ColumnName.COMMENT_ID)
      .references(ColumnName.ID)
      .inTable(TableName.COMMENTS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
  });
}

export async function down(knex) {
  await knex.schema.dropTableIfExists(TableName.COMMENT_REACTIONS);
}
