import { HttpMethod, ContentType } from 'common/enums/enums';

class Comment {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load(`${this._apiPath}/comments`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(id, payload) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.DELETE,
      contentType: ContentType.JSON
    });
  }

  reactComment(commentId, isLike = true) {
    return this._http.load(`${this._apiPath}/comments/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike
      })
    });
  }

  likeComment(commentId) {
    return this.reactComment(commentId);
  }

  dislikeComment(commentId) {
    return this.reactComment(commentId, false);
  }
}

export { Comment };
