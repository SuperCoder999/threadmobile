import { ExceptionMessage, EmailTemplate } from '../../common/enums/enums';
import { InvalidCredentialsError, InvalidResetPasswordTokenError, NotFoundError } from '../../exceptions/exceptions';
import {
  createToken,
  cryptCompare,
  encrypt,
  verifyToken,
  createResetToken,
  getResetTokenExpiration,
  verifyResetToken,
  getDeepLink
} from '../../helpers/helpers';
import { celery } from '../../celery/client/client';

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async verifyLoginCredentials({ email, password }) {
    const user = await this._userRepository.getByEmail(email);

    if (!user) {
      throw new InvalidCredentialsError(ExceptionMessage.INCORRECT_EMAIL);
    }

    const isEqualPassword = await cryptCompare(password, user.password);
    if (!isEqualPassword) {
      throw new InvalidCredentialsError(ExceptionMessage.PASSWORDS_NOT_MATCH);
    }

    return user;
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const { email, username } = userData;

    const userByEmail = await this._userRepository.getByEmail(email);
    if (userByEmail) {
      throw new InvalidCredentialsError(ExceptionMessage.EMAIL_ALREADY_EXISTS);
    }

    const userByUsername = await this._userRepository.getByUsername(username);
    if (userByUsername) {
      throw new InvalidCredentialsError(
        ExceptionMessage.USERNAME_ALREADY_EXISTS
      );
    }

    const newUser = await this._userRepository.create({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async requestResetPassword(userEmail) {
    const user = await this._userRepository.getByEmail(userEmail);

    if (!user) {
      throw new NotFoundError('User');
    }

    const { token, salt } = createResetToken(user.id);
    const link = getDeepLink(`reset-password/${token}`);

    await this._userRepository.updateById(user.id, {
      resetPasswordToken: await encrypt(salt),
      resetPasswordTokenExpires: getResetTokenExpiration().toUTCString()
    });

    celery.runTask('sendEmail', {
      to: user.email,
      template: EmailTemplate.RESET_PASSWORD,
      variables: {
        username: user.username,
        link
      }
    });
  }

  async resetPassword(token, newPassword) {
    const user = await this.checkResetToken(token);

    await this._userRepository.updateById(user.id, {
      password: await encrypt(newPassword),
      resetPasswordToken: null,
      resetPasswordTokenExpires: null
    });
  }

  async checkResetToken(token) {
    const { id, salt } = verifyResetToken(token);
    const user = await this._userRepository.getById(id);

    if (!user.resetPasswordToken) {
      throw new InvalidResetPasswordTokenError();
    }

    const validSalt = await cryptCompare(salt, user.resetPasswordToken);
    const expired = new Date() > new Date(user.resetPasswordTokenExpires);

    if (!validSalt || expired) {
      throw new InvalidResetPasswordTokenError();
    }

    return user;
  }

  async verifyToken(token) {
    return verifyToken(token);
  }
}

export { Auth };
