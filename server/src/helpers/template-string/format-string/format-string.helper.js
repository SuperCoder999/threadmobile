const formatString = (string, variables) => {
  const result = string.replace(
    /\{\{\s*([a-zA-Z0-9-_]+)\s*\}\}/g,
    (_, variableName) => variables[variableName]
  );

  return result;
};

export { formatString };
