import { UserPayloadKey } from 'common/enums/enums';

const DEFAULT_RESET_PAYLOAD = {
  [UserPayloadKey.PASSWORD]: ''
};

export { DEFAULT_RESET_PAYLOAD };
