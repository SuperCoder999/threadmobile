# BSA 2021 React Native - mini-project Thread

## Описание

[**Thread**](https://bitbucket.org/SuperCoder999/threadmobile.git) - это приложение с готовой архитектурой и структурой, подключенным базовым стеком технологий и стартовым функционалом, предназначенный для самостоятельной практики студентов.

Тематика проекта - социальная сеть, похожая на Twitter.

Основная идея проекта - ознакомить студентов с нашим виденьем того, как реальный проект должен выглядеть изнутри, и дать возможность самостоятельно исследовать, как устроена архитектура и структура проекта, посмотреть его возможные конфигурации, попробовать покопаться и разобраться в чужом коде.

## Технологии

Здесь перечислены основные фреймворки и библиотеки, используемые в проекте. Полный список используемых технологий для каждой части проекта находится в файлах package.json в папках client и server.

### Common

1. ES2021
2. [Git](https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git 'Git')
3. [REST API](https://www.restapitutorial.com/lessons/restquicktips.html 'REST API')
4. [JWT](https://en.wikipedia.org/wiki/JSON_Web_Token 'JWT')
5. [Socket.IO](https://socket.io/docs/ 'Socket.IO')
6. [npm](<https://en.wikipedia.org/wiki/Npm_(software)>)
7. [ESLint](https://eslint.org/docs/user-guide/getting-started 'ESLint')

### Frontend

1. [React Native](https://reactnative.dev/docs/0.64/getting-started 'React Native')
2. [Redux](https://redux.js.org/introduction/getting-started 'Redux')
3. [React Navigation](https://reactnavigation.org/docs/getting-started 'React Navigation')
4. [dayjs](https://day.js.org/ 'day.js')
5. [React Hook Form](https://react-hook-form.com/get-started 'React Hook Form')
6. [joi](https://www.npmjs.com/package/joi 'joi')

### Backend

1. [Node.js](https://nodejs.org/en/ 'Node.js')
2. [Fastify](https://www.fastify.io/docs/v3.24.x/ 'Fastify')
3. [Knex](https://knexjs.org/ 'Knex')
4. [Objection](https://vincit.github.io/objection.js/ 'Objection')
5. [axios](https://www.npmjs.com/package/axios 'axios')
6. [bcrypt](https://www.npmjs.com/package/bcrypt 'bcrypt')
7. [Babel](https://babeljs.io/docs/en/index.html 'Babel')
8. [nodemon](https://www.npmjs.com/package/nodemon 'nodemon')
9. [dotenv](https://www.npmjs.com/package/dotenv 'dotenv')
10. [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken 'jsonwebtoken')
11. [Celery](https://docs.celeryproject.org/en/stable 'Celery') using [celery-node](https://www.npmjs.com/package/celery-node 'celery-node')
12. [Redis](https://redis.io 'Redis') (used as Celery broker)

### Database

1. [PostgreSQL](https://www.postgresql.org/download/ 'PostgreSQL') (inside Docker)
2. [Adminer](https://www.adminer.org 'Adminer') (inside Docker)

## Установка

1. Установить последнюю стабильную версию [Node.js](https://nodejs.org/en/ 'Node.js') (LTS). **Note:** npm будет установлен автоматически. Проверьте корректность установки: для этого выполните в командной строке (терминале):

   ```bash
   node -v  // для проверки версии Node.js
   npm -v // для проверки версии npm
   ```

2. Установить последнюю стабильную версию [Docker](https://www.docker.com/products/ 'Docker') для вашей OS.

3. Установите Git.

4. Склонировать [репозиторий](https://bitbucket.org/SuperCoder999/threadmobile.git) проекта:

   ```bash
   git clone https://bitbucket.org/SuperCoder999/threadmobile.git
   ```

### Корень проекта

1. В руте проекта можно установить все зависимости одной командой:

   ```bash
   npm run install:all
   ```

   Это установит зависимости для рутовой директории, frontend'а и backend'a. Можно установить для каждой папки отедьно (смотрите ниже).

2. После установки пакетов, в руте проекта нужно запустить команду для [git-хуков](https://www.npmjs.com/package/simple-git-hooks):

   ```bash
   npx simple-git-hooks
   ```

   **Теперь на каждый ваш коммит будет запускаться линтер и проверять ваш код.**

### Backend

1. В командной строке (терминале) зайдите в папку server:

   ```bash
   cd /* путь к папке server */
   ```

2. Установите все необходимы пакеты из package.json командой:

   ```bash
   npm install
   ```

3. В папке server создайте файл **.env** и скопируйте в него содержимое из файла **.env.example**. Сделайте то же самое для **.db.env** и **.db.env.example**.

   **Note**: файл **.env** содержит реальные ключи проекта и не должен сохраняться в репозиторий.

   Замените в файле **.env** значения ключей на действительные.
   Для того, чтобы указать ключи для Gyazo Storage, необходимо зарегистрироваться на сайте [Gyazo](https://gyazo.com/captures) и [зарегистрировать приложение](https://gyazo.com/oauth/applications). Затем в **.env** использовать `access token` из только что созданного приложения в Gyazo.
   Для того, чтобы указать ключи для Google Cloud, нужно создать их как показано [в этой статье](https://alexb72.medium.com/how-to-send-emails-using-a-nodemailer-gmail-and-oauth2-fe19d66451f9)

4. Запустите PostgreSQL, Redis и Adminer командой:

   ```bash
   docker-compose up -d
   ```

5. Создайте в PostgreSQL **пустую** базу данных для проекта, если она не создалась автоматически. Для этого [откройте Adminer](http://localhost:8080) и подключитесь к серверу PostgreSQL. Логин и пароль находятся в файле **.db.env**.

6. Выполните [миграции](https://knexjs.org/#Migrations) и сиды для того, чтобы заполнить базу данных демо-данными. Для этого в командной строке (терминале) в папке server выполните:

   ```bash
   npm run migrate:run
   npm run seed:run
   ```

Проверьте базу данных на наличие демо-данных.

7. Для запуска сервера в командной строке (терминале) в папке сервера выполните:

   ```bash
   npm start
   ```

8. Для запуска Celery-worker'а, необходимого для отправки почты, выполните:

   ```bash
   npm run start:celery
   ```

### Frontend

1. Настройте окружение следуя [документации React Native](https://reactnative.dev/docs/environment-setup)

2. В командной строке (терминале) зайдите в папку client:

   ```bash
   cd /* путь к папке client */
   ```

3. Установите все необходимы пакеты из package.json командой:

   ```bash
   npm install
   ```

4. В папке client создайте файлы **.env.android** и **.env.ios**, скопируйте в них содержимое из соответствующих файлов **.env.example**.

   **Note**: файлы **.env** содержат реальные ключи проекта и не должны сохраняться в репозиторий.

   Замените в файлах **.env** значения ключей на действительные.

5. Для запуска клиента в командной строке (терминале) в папке клиента выполните:

   ```bash
   npx react-native start
   ```

   **Note**: в случае изменения .env файла при запуске команды нужно добавить флаг `--reset-cache`:

   ```bash
   npx react-native start --reset-cache
   ```

   Откройте новый терминал и из папки клиента запустите одну из следующих команд:

   ```bash
   npx react-native run-android
   ```

   ```bash
   npx react-native run-ios
   ```

## Задания

Весь список тасков и багов можно найти на доске [**Trello**](https://trello.com/b/gf4sndOM/thread-mobile '**Trello**') в колонке **Done**.

Ссылки:

1. [Репозиторий](https://bitbucket.org/SuperCoder999/threadmobile.git).
2. [Trello](https://trello.com/b/gf4sndOM/thread-mobile).
