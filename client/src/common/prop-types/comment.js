import PropTypes from 'prop-types';
import { shortUserType } from 'common/prop-types/user';

const commentType = PropTypes.exact({
  id: PropTypes.number.isRequired,
  body: PropTypes.string.isRequired,
  likeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  dislikeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  userId: PropTypes.number.isRequired,
  user: shortUserType.isRequired
});

export { commentType };
