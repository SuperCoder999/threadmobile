import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (router, opts, done) => {
  const { comment: commentService } = opts.services;

  router
    .get(CommentsApiPath.$ID, req => commentService.getCommentById(req.params.id))
    .post(CommentsApiPath.ROOT, req => commentService.create(req.user.id, req.body))
    .put(CommentsApiPath.REACT, req => commentService.setReaction(req.user.id, req.body))
    .patch(CommentsApiPath.$ID, req => commentService.update(req.params.id, req.body))
    .delete(CommentsApiPath.$ID, req => commentService.delete(req.params.id));

  done();
};

export { initComment };
