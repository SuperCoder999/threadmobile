import * as React from 'react';
import { NotificationMessage, ButtonVariant, IconName, UserPayloadKey } from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { Button, Image, Input, Stack, View, ScrollView } from 'components/components';
import { useAppForm, useDispatch, useSelector, useState } from 'hooks/hooks';
import { pickImage } from 'helpers/helpers';
import { profileActionCreator } from 'store/actions';
import { profileUpdate as profileUpdateValidationSchema } from 'validation-schemas/validation-schemas';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import styles from './styles';

const Profile = () => {
  const dispatch = useDispatch();

  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [image, setImage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isUploadingImage, setIsUploadingImage] = useState(false);

  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: {
      [UserPayloadKey.USERNAME]: user?.username,
      [UserPayloadKey.STATUS]: user?.status,
      [UserPayloadKey.EMAIL]: user?.email
    },
    validationSchema: profileUpdateValidationSchema
  });

  const handleUserLogout = () => dispatch(profileActionCreator.logout());

  const handleProfileUpdate = values => {
    if (errors.length || isUploadingImage) {
      return;
    }

    setIsLoading(true);

    const payload = {
      username: values[UserPayloadKey.USERNAME],
      status: values[UserPayloadKey.STATUS],
      imageId: image?.id ?? user?.imageId
    };

    dispatch(profileActionCreator.update(payload))
      .unwrap()
      .finally(() => setIsLoading(false));

    setImage(undefined);
  };

  const handleUploadFile = async () => {
    setIsUploadingImage(true);

    try {
      const data = await pickImage();

      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploadingImage(false);
    }
  };

  if (!user) {
    return null;
  }

  return (
    <View style={styles.screen}>
      <ScrollView
        style={styles.content}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      >
        <Image
          style={styles.avatar}
          accessibilityIgnoresInvertColors
          source={{ uri: image?.link ?? user.image?.link ?? DEFAULT_USER_AVATAR }}
        />
        <Stack space={15}>
          <Button
            title="Upload profile image"
            variant={ButtonVariant.TEXT}
            icon={IconName.PLUS_SQUARE}
            isLoading={isUploadingImage}
            onPress={handleUploadFile}
          />
          <Input
            name={UserPayloadKey.USERNAME}
            control={control}
            errors={errors}
            iconName={IconName.USER}
          />
          <Input
            name={UserPayloadKey.STATUS}
            placeholder="Set user status..."
            control={control}
            errors={errors}
            iconName={IconName.COMMENT}
          />
          <Input
            name={UserPayloadKey.EMAIL}
            control={control}
            errors={errors}
            iconName={IconName.ENVELOPE}
            isDisabled
          />
          <Button
            title="Update profile"
            isLoading={isLoading}
            isDisabled={isUploadingImage}
            onPress={handleSubmit(handleProfileUpdate)}
          />
          <Button
            title="Logout"
            variant={ButtonVariant.TEXT}
            onPress={handleUserLogout}
          />
        </Stack>
      </ScrollView>
    </View>
  );
};

export default Profile;
