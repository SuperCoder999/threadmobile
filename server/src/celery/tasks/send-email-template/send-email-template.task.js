import { gmail } from '../../../smtp/smtp';

const name = 'sendEmail';

const sendEmailTemplate = async ({ to, template, variables }) => {
  await gmail.sendTemplate(to, template, variables);
};

export { name, sendEmailTemplate as func };
