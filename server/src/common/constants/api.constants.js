import { ApiPath, AuthApiPath, ENV } from '../enums/enums';

const WHITE_ROUTES = [
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.LOGIN}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.REGISTER}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.REQUEST_RESET}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.RESET}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.CHECK_RESET}`,
  `${ENV.APP.API_PATH}${ApiPath.DEEP_LINKS}`
];

export { WHITE_ROUTES };
