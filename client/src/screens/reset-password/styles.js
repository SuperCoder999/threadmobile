import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    width: '100%',
    maxWidth: 330,
    paddingTop: 20,
    paddingBottom: 60,
    paddingHorizontal: 15
  },
  header: {
    paddingBottom: 40,
    textAlign: 'center'
  }
});

export default styles;
