const UserPayloadKey = {
  USERNAME: 'username',
  STATUS: 'status',
  EMAIL: 'email',
  PASSWORD: 'password'
};

export { UserPayloadKey };
