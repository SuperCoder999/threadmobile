const ActionType = {
  LOG_IN: 'profile/log-in',
  LOG_OUT: 'profile/log-out',
  REGISTER: 'profile/register',
  UPDATE: 'profile/update',
  REQUEST_RESET_PASSWORD: 'profile/reset-password/request',
  RESET_PASSWORD: 'profile/reset-password',
  CHECK_RESET_PASSWORD_TOKEN: 'profile/reset-password/check'
};

export { ActionType };
