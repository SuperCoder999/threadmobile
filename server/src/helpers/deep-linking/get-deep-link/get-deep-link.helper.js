import { ENV } from '../../../common/enums/enums';

const getDeepLink = path => {
  const link = `${ENV.APP.LINK_PREFIX}${path}`;
  const encodedLink = Buffer.from(link).toString('base64');
  const fullLink = `${ENV.APP.LINK_MASK}?link=${encodedLink}`;

  return fullLink;
};

export { getDeepLink };
