import { HttpMethod, ContentType } from 'common/enums/enums';

class Profile {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  update(payload) {
    return this._http.load(`${this._apiPath}/profile/update`, {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { Profile };
