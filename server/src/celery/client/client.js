import { createClient } from 'celery-node';
import { ENV } from '../../common/enums/enums';
import { registerClientTasks } from '../tasks/tasks';

class Celery {
  constructor() {
    this._connect();
  }

  _connect() {
    if (this.connection) {
      return;
    }

    this.connection = createClient(ENV.CELERY.BROKER, ENV.CELERY.BACKEND);
    this.tasks = registerClientTasks(this.connection);
  }

  runTask(name, arg) {
    return this.tasks[name].applyAsync([arg]);
  }

  runTaskAndGetResult(name, arg) {
    return this.runTask(name, arg).get();
  }
}

const celery = new Celery();

export { celery };
