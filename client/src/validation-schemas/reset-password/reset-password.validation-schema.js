import {
  UserPayloadKey,
  UserValidationMessage,
  UserValidationRule
} from 'common/enums/enums';
import { Joi } from 'helpers/helpers';

const resetPassword = Joi.object({
  [UserPayloadKey.PASSWORD]: Joi.string()
    .trim()
    .min(UserValidationRule.PASSWORD_MIN_LENGTH)
    .max(UserValidationRule.PASSWORD_MAX_LENGTH)
    .required()
    .messages({
      'string.empty': UserValidationMessage.PASSWORD_REQUIRE,
      'string.min': UserValidationMessage.PASSWORD_MIN_LENGTH,
      'string.max': UserValidationMessage.PASSWORD_MAX_LENGTH
    })
});

export { resetPassword };
