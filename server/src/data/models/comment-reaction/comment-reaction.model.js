import { Model } from 'objection';

import { DbTableName } from '../../../common/enums/enums';
import AbstractModel from '../abstract/abstract.model';
import CommentModel from '../comment/comment.model';
import UserModel from '../user/user.model';

class CommentReaction extends AbstractModel {
  static get tableName() {
    return DbTableName.COMMENT_REACTIONS;
  }

  static get jsonSchema() {
    const baseSchema = super.jsonSchema;

    return {
      type: baseSchema.type,
      required: ['isLike', 'userId', 'commentId'],
      properties: {
        ...baseSchema.properties,
        isLike: { type: 'boolean' },
        commentId: { type: ['integer', 'null'] },
        userId: { type: ['integer', 'null'] }
      }
    };
  }

  static get relationMappings() {
    return {
      comment: {
        relation: Model.HasOneRelation,
        modelClass: CommentModel,
        join: {
          from: `${DbTableName.COMMENT_REACTIONS}.commentId`,
          to: `${DbTableName.COMMENTS}.id`
        }
      },
      user: {
        relation: Model.HasOneRelation,
        modelClass: UserModel,
        filter: query => query.select('id', 'userId'),
        join: {
          from: `${DbTableName.COMMENT_REACTIONS}.userId`,
          to: `${DbTableName.USERS}.id`
        }
      }
    };
  }
}

export default CommentReaction;
