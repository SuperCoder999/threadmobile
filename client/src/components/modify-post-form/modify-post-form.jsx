import * as React from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';
import {
  ButtonVariant,
  IconName,
  NotificationMessage
} from 'common/enums/enums';
import { Button, Image, View } from 'components/components';
import { AppColor } from 'config/config';
import { pickImage } from 'helpers/helpers';
import { useState } from 'hooks/hooks';
import { postType } from 'common/prop-types/prop-types';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import styles from './styles';

const ModifyPostForm = ({ defaultPost, onSubmit }) => {
  const [body, setBody] = useState(defaultPost?.body ?? '');
  const [image, setImage] = useState(defaultPost?.image);
  const [isUploading, setIsUploading] = useState(false);

  const handleUploadFile = async () => {
    setIsUploading(true);

    try {
      const data = await pickImage();
      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  const handleSubmit = () => {
    if (!body) {
      return;
    }

    onSubmit({ imageId: image?.id, body });
    setBody('');
    setImage(undefined);
  };

  return (
    <>
      <TextInput
        multiline
        value={body}
        placeholder="Type something here..."
        placeholderTextColor={AppColor.PLACEHOLDER}
        numberOfLines={10}
        style={styles.input}
        onChangeText={setBody}
      />
      <View style={styles.buttonWrapper}>
        <Button
          title="Attach image"
          variant={ButtonVariant.TEXT}
          icon={IconName.PLUS_SQUARE}
          isLoading={isUploading}
          onPress={handleUploadFile}
        />
      </View>
      {image?.link && (
        <Image
          style={styles.image}
          accessibilityIgnoresInvertColors
          source={{ uri: image?.link }}
        />
      )}
      <Button
        title={defaultPost ? 'Update' : 'Add'}
        isDisabled={!body || isUploading}
        onPress={handleSubmit}
      />
    </>
  );
};

ModifyPostForm.propTypes = {
  defaultPost: postType,
  onSubmit: PropTypes.func.isRequired
};

ModifyPostForm.defaultProps = {
  defaultPost: null
};

export default ModifyPostForm;
